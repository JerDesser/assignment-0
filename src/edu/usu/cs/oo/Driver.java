package edu.usu.cs.oo;

public class Driver {
	
	public static void main(String[] args)
	{
		Student student = new Student("Chris Thatcher", "AOneMillion", new Job("Dentist", 100000, new Company()));
		Student Jeremy = new Student("Jeremy Desser", "A55555555", new Job("Superhero", 99999999, new Company()));
		
		/*
		 * Instantiate an instance of Student that includes your own personal information
		 * Feel free to make up information if you would like.
		 */
		
		
		System.out.println(student);
		System.out.println(Jeremy);
		
		/*
		 * Print out the student information. 
		 */
	}

}