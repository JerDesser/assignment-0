package edu.usu.cs.oo;

public class Company {

	private String name;
	private Location location;
	
public Company(){
		
	name = "unknown";
	
		
	}
	public Company(String name, Location location){
		
		this.name = name;
		this.location = location;
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public String toString(){
		
		return "Company: "+ name +" Located: " + location;
	}
	
	/*
	 * Create the constructor, getters, setters, and anything else
	 * that is necessary to make Company work.
	 * 
	 * Note: This includes the toString() method.
	 */
}
